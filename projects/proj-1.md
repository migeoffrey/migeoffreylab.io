---
layout: post
title: 'Humains'
image: 
    - image_path: /assets/img/projects/proj-1/P1.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 1
    - image_path: /assets/img/projects/proj-1/P2.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 2
    - image_path: /assets/img/projects/proj-1/P3.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 3
    - image_path: /assets/img/projects/proj-1/P4.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 4
    - image_path: /assets/img/projects/proj-1/P5.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 5
    - image_path: /assets/img/projects/proj-1/P6.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 6
    - image_path: /assets/img/projects/proj-1/P7.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 7
    - image_path: /assets/img/projects/proj-1/P8.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 8
      image-vertical: 1
    - image_path: /assets/img/projects/proj-1/P9.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 9
    - image_path: /assets/img/projects/proj-1/P10.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 10
    - image_path: /assets/img/projects/proj-1/P11.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 11
    - image_path: /assets/img/projects/proj-1/P12.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 12
    - image_path: /assets/img/projects/proj-1/P13.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 13
    - image_path: /assets/img/projects/proj-1/P14.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 14
    - image_path: /assets/img/projects/proj-1/P15.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 15
    - image_path: /assets/img/projects/proj-1/P16.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 16
    - image_path: /assets/img/projects/proj-1/P17.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 17
---

