---
layout: post
title: 'Insects'
image: 
    - image_path: /assets/img/projects/proj-3/P1.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 1
    - image_path: /assets/img/projects/proj-3/P2.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 2
    - image_path: /assets/img/projects/proj-3/P3.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 3
    - image_path: /assets/img/projects/proj-3/P4.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 4
    - image_path: /assets/img/projects/proj-3/P5.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 5
    - image_path: /assets/img/projects/proj-3/P6.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 6
    - image_path: /assets/img/projects/proj-3/P7.jpg
      image-caption: IMAGE TITLE
      image-copyright: © Geoffrey Migault
      image-number: 7
---
